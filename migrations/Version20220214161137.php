<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220214161137 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_62354740A76ED395');
        $this->addSql('DROP INDEX IDX_62354740E7EC5785');
        $this->addSql('CREATE TEMPORARY TABLE __temp__anti_message AS SELECT id, board_id, user_id, image, content, datetime, topic, password FROM anti_message');
        $this->addSql('DROP TABLE anti_message');
        $this->addSql('CREATE TABLE anti_message (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, board_id INTEGER NOT NULL, user_id INTEGER NOT NULL, image VARCHAR(255) DEFAULT NULL COLLATE BINARY, content CLOB DEFAULT NULL COLLATE BINARY, datetime DATETIME NOT NULL, topic BOOLEAN NOT NULL, password VARCHAR(255) DEFAULT NULL COLLATE BINARY, CONSTRAINT FK_62354740E7EC5785 FOREIGN KEY (board_id) REFERENCES board (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_62354740A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO anti_message (id, board_id, user_id, image, content, datetime, topic, password) SELECT id, board_id, user_id, image, content, datetime, topic, password FROM __temp__anti_message');
        $this->addSql('DROP TABLE __temp__anti_message');
        $this->addSql('CREATE INDEX IDX_62354740A76ED395 ON anti_message (user_id)');
        $this->addSql('CREATE INDEX IDX_62354740E7EC5785 ON anti_message (board_id)');
        $this->addSql('DROP INDEX IDX_4D37D92E11E48FAC');
        $this->addSql('DROP INDEX IDX_4D37D92E801DF23');
        $this->addSql('CREATE TEMPORARY TABLE __temp__anti_message_anti_message AS SELECT anti_message_source, anti_message_target FROM anti_message_anti_message');
        $this->addSql('DROP TABLE anti_message_anti_message');
        $this->addSql('CREATE TABLE anti_message_anti_message (anti_message_source INTEGER NOT NULL, anti_message_target INTEGER NOT NULL, PRIMARY KEY(anti_message_source, anti_message_target), CONSTRAINT FK_4D37D92E801DF23 FOREIGN KEY (anti_message_source) REFERENCES anti_message (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_4D37D92E11E48FAC FOREIGN KEY (anti_message_target) REFERENCES anti_message (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO anti_message_anti_message (anti_message_source, anti_message_target) SELECT anti_message_source, anti_message_target FROM __temp__anti_message_anti_message');
        $this->addSql('DROP TABLE __temp__anti_message_anti_message');
        $this->addSql('CREATE INDEX IDX_4D37D92E11E48FAC ON anti_message_anti_message (anti_message_target)');
        $this->addSql('CREATE INDEX IDX_4D37D92E801DF23 ON anti_message_anti_message (anti_message_source)');
        $this->addSql('DROP INDEX IDX_62FED0E5A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ban AS SELECT id, user_id, start, "end" FROM ban');
        $this->addSql('DROP TABLE ban');
        $this->addSql('CREATE TABLE ban (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER NOT NULL, start DATETIME NOT NULL, "end" DATETIME DEFAULT NULL, CONSTRAINT FK_62FED0E5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO ban (id, user_id, start, "end") SELECT id, user_id, start, "end" FROM __temp__ban');
        $this->addSql('DROP TABLE __temp__ban');
        $this->addSql('CREATE INDEX IDX_62FED0E5A76ED395 ON ban (user_id)');
        $this->addSql('ALTER TABLE page ADD COLUMN display_menu BOOLEAN');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_62354740E7EC5785');
        $this->addSql('DROP INDEX IDX_62354740A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__anti_message AS SELECT id, board_id, user_id, image, content, datetime, topic, password FROM anti_message');
        $this->addSql('DROP TABLE anti_message');
        $this->addSql('CREATE TABLE anti_message (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, board_id INTEGER NOT NULL, user_id INTEGER NOT NULL, image VARCHAR(255) DEFAULT NULL, content CLOB DEFAULT NULL, datetime DATETIME NOT NULL, topic BOOLEAN NOT NULL, password VARCHAR(255) DEFAULT NULL)');
        $this->addSql('INSERT INTO anti_message (id, board_id, user_id, image, content, datetime, topic, password) SELECT id, board_id, user_id, image, content, datetime, topic, password FROM __temp__anti_message');
        $this->addSql('DROP TABLE __temp__anti_message');
        $this->addSql('CREATE INDEX IDX_62354740E7EC5785 ON anti_message (board_id)');
        $this->addSql('CREATE INDEX IDX_62354740A76ED395 ON anti_message (user_id)');
        $this->addSql('DROP INDEX IDX_4D37D92E801DF23');
        $this->addSql('DROP INDEX IDX_4D37D92E11E48FAC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__anti_message_anti_message AS SELECT anti_message_source, anti_message_target FROM anti_message_anti_message');
        $this->addSql('DROP TABLE anti_message_anti_message');
        $this->addSql('CREATE TABLE anti_message_anti_message (anti_message_source INTEGER NOT NULL, anti_message_target INTEGER NOT NULL, PRIMARY KEY(anti_message_source, anti_message_target))');
        $this->addSql('INSERT INTO anti_message_anti_message (anti_message_source, anti_message_target) SELECT anti_message_source, anti_message_target FROM __temp__anti_message_anti_message');
        $this->addSql('DROP TABLE __temp__anti_message_anti_message');
        $this->addSql('CREATE INDEX IDX_4D37D92E801DF23 ON anti_message_anti_message (anti_message_source)');
        $this->addSql('CREATE INDEX IDX_4D37D92E11E48FAC ON anti_message_anti_message (anti_message_target)');
        $this->addSql('DROP INDEX IDX_62FED0E5A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ban AS SELECT id, user_id, start, "end" FROM ban');
        $this->addSql('DROP TABLE ban');
        $this->addSql('CREATE TABLE ban (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER NOT NULL, start DATETIME NOT NULL, "end" DATETIME DEFAULT NULL)');
        $this->addSql('INSERT INTO ban (id, user_id, start, "end") SELECT id, user_id, start, "end" FROM __temp__ban');
        $this->addSql('DROP TABLE __temp__ban');
        $this->addSql('CREATE INDEX IDX_62FED0E5A76ED395 ON ban (user_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__page AS SELECT id, title, url, content, display_in_menu FROM page');
        $this->addSql('DROP TABLE page');
        $this->addSql('CREATE TABLE page (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title CLOB NOT NULL, url VARCHAR(255) NOT NULL, content CLOB DEFAULT NULL, display_in_menu BOOLEAN NOT NULL)');
        $this->addSql('INSERT INTO page (id, title, url, content, display_in_menu) SELECT id, title, url, content, display_in_menu FROM __temp__page');
        $this->addSql('DROP TABLE __temp__page');
    }
}
