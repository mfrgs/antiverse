<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220209085817 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE admin (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_880E0D765E237E06 ON admin (name)');
        $this->addSql('CREATE TABLE anti_message (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, board_id INTEGER NOT NULL, user_id INTEGER NOT NULL, image VARCHAR(255) DEFAULT NULL, content CLOB DEFAULT NULL, datetime DATETIME NOT NULL, topic BOOLEAN NOT NULL, password VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_62354740E7EC5785 ON anti_message (board_id)');
        $this->addSql('CREATE INDEX IDX_62354740A76ED395 ON anti_message (user_id)');
        $this->addSql('CREATE TABLE anti_message_anti_message (anti_message_source INTEGER NOT NULL, anti_message_target INTEGER NOT NULL, PRIMARY KEY(anti_message_source, anti_message_target))');
        $this->addSql('CREATE INDEX IDX_4D37D92E801DF23 ON anti_message_anti_message (anti_message_source)');
        $this->addSql('CREATE INDEX IDX_4D37D92E11E48FAC ON anti_message_anti_message (anti_message_target)');
        $this->addSql('CREATE TABLE ban (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER NOT NULL, start DATETIME NOT NULL, "end" DATETIME DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_62FED0E5A76ED395 ON ban (user_id)');
        $this->addSql('CREATE TABLE board (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, nickname VARCHAR(255) NOT NULL, header CLOB DEFAULT NULL, whitelisted BOOLEAN NOT NULL)');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, ip VARCHAR(255) NOT NULL, password VARCHAR(255) DEFAULT NULL, whitelisted BOOLEAN NOT NULL)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE admin');
        $this->addSql('DROP TABLE anti_message');
        $this->addSql('DROP TABLE anti_message_anti_message');
        $this->addSql('DROP TABLE ban');
        $this->addSql('DROP TABLE board');
        $this->addSql('DROP TABLE user');
    }
}
