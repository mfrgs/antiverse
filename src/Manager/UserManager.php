<?php
// src/Service/UserManager.php
namespace App\Manager;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\User;
use App\Entity\Board;
use App\Manager\AntiMessageManager;
use Symfony\Contracts\Translation\TranslatorInterface;


class UserManager
{

  private $repository;
  private $entityManager;

  public function __construct(ManagerRegistry $doctrine, private AntiMessageManager $antiMessageManager, private TranslatorInterface $translator)
  {
    $this->repository = $doctrine->getRepository(User::class);
    $this->entityManager = $doctrine->getManager();
  }

  /*
    Check if user had authorisation or register him if new
  */
  function authenticate(String $ip, $board) : Array
  {

    $response = array("user" => null, "message" => null);
    // if board has whitelist activated
    if($board->getWhitelisted() && !empty($this->repository->findOneBy(["ip" => $ip, "whitelisted" => false]))){
      $response["message"] = $this->translator->trans('whitelist');
      return $response;
    }else{
      $response["user"] = $this->register($ip);
    }
    $bans = $this->repository->isBanned($response["user"]->getId());
    //if user is banned
    if(!empty($bans)){
      $response["user"] = $response["user"];
      $date = $bans[0]["end"]->format('d/m/Y H:i:s');
      $response["message"] = $bans[0]["reason"];
    }
    return $response;
  }

  /*
    Try to find if user exist else create him
  */
  public function register(String $ip) :User
  {
    $user = $this->exist($ip);
    if(!$user)
    {
      $user = $this->createUser($ip);
    }
    return $user;
  }

  public function exist(String $ip)
  {
    $user = $this->repository->findOneBy(['ip' => $ip]);
    if($user !== null){
      return $user;
    }else{
      return false;
    }
  }

  public function createUser($ip): User
  {
    $user = new User();
    $user->setName($ip);
    $user->setIp($ip);
    $user->setWhitelisted(false);
    $this->entityManager->persist($user);
    $this->entityManager->flush();
    return $user;
  }

  /*
    Delete all messages and bans associated
  */
  public function deleteUser($user) :void
  {
    foreach ($user->getAntiMessages() as $antiMessage) {
      $this->antiMessageManager->deleteAntimessages($antiMessage->getId(),"none");
    }
    foreach ($user->getBans() as $ban) {
      $this->entityManager->remove($ban);
    }
      $this->entityManager->remove($user);
      $this->entityManager->flush();
  }
}
