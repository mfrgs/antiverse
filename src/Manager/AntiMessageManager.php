<?php
// src/Service/UserManager.php
namespace App\Manager;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\AntiMessage;
use Symfony\Component\Security\Core\Security;


class AntiMessageManager
{
  private $repository;
  private $entityManager;

  public function __construct(ManagerRegistry $doctrine, private Security $security)
  {
    $this->repository = $doctrine->getRepository(AntiMessage::class);
    $this->entityManager = $doctrine->getManager();
  }

  public function deleteAntimessages(int $id, $password) :void
  {
    $antiMessage = $this->repository->find($id);
    if (($password == $antiMessage->getPassword() && $antiMessage->getPassword() != null) || $this->security->isGranted('ROLE_ADMIN')){
      if (count($antiMessage->getResponses()) > 0){
        foreach ($antiMessage->getResponses() as $response) {
          $this->entityManager->remove($response);
        }
      }
      $this->entityManager->remove($antiMessage);
   }
    $this->entityManager->flush();
  }
}
