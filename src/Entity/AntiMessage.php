<?php

namespace App\Entity;

use App\Repository\AntiMessageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AntiMessageRepository::class)]
class AntiMessage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $image;

    #[ORM\Column(type: 'text', nullable: true)]
    private $content;

    #[ORM\Column(type: 'datetime')]
    private $datetime;

    #[ORM\ManyToOne(targetEntity: Board::class, inversedBy: 'antiMessages')]
    #[ORM\JoinColumn(nullable: false)]
    private $board;

    #[ORM\Column(type: 'boolean')]
    private $topic;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'antiMessages')]
    #[ORM\JoinColumn(nullable: false)]
    private $user;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $password;

    #[ORM\ManyToMany(targetEntity: self::class, inversedBy: 'responses')]
    private $responses;

    public function __construct()
    {
        $this->responses = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getBoard(): ?Board
    {
        return $this->board;
    }

    public function setBoard(?Board $board): self
    {
        $this->board = $board;

        return $this;
    }


    public function getTopic(): ?bool
    {
        return $this->topic;
    }

    public function setTopic(bool $topic): self
    {
        $this->topic = $topic;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getResponses(): Collection
    {
        return $this->responses;
    }

    public function addResponse(self $response): self
    {
        if (!$this->responses->contains($response)) {
            $this->responses[] = $response;
        }

        return $this;
    }

    public function removeResponse(self $response): self
    {
        $this->responses->removeElement($response);

        return $this;
    }

    public function getResponsesLimit($limit = 5) :Collection
    {
      $criteria = \Doctrine\Common\Collections\Criteria::create()
          ->orderBy(array('datetime'=> \Doctrine\Common\Collections\Criteria::DESC))
          ->setMaxResults($limit);
          return $this->responses->matching($criteria);
    }

    public function setResponses(Collection $responses){
      $this->responses = $responses;
      return $this;

    }
}
