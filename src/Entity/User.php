<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserRepository::class)]
class User
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    private $ip;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: AntiMessage::class)]
    private $antiMessages;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Ban::class)]
    private $bans;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $password;

    #[ORM\Column(type: 'boolean')]
    private $whitelisted;


    public function __construct()
    {
        $this->antiMessages = new ArrayCollection();
        $this->bans = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }


    /**
     * @return Collection|AntiMessage[]
     */
    public function getAntiMessages(): Collection
    {
        return $this->antiMessages;
    }

    public function addAntiMessage(AntiMessage $antiMessage): self
    {
        if (!$this->antiMessages->contains($antiMessage)) {
            $this->antiMessages[] = $antiMessage;
            $antiMessage->setUser($this);
        }

        return $this;
    }

    public function removeAntiMessage(AntiMessage $antiMessage): self
    {
        // set the owning side to null (unless already changed)
        if ($this->antiMessages->removeElement($antiMessage) && $antiMessage->getUser() === $this) {
            $antiMessage->setUser(null);
        }

        return $this;
    }

    /**
     * @return Collection|Ban[]
     */
    public function getBans(): Collection
    {
        return $this->bans;
    }

    public function addBan(Ban $ban): self
    {
        if (!$this->bans->contains($ban)) {
            $this->bans[] = $ban;
            $ban->setUser($this);
        }

        return $this;
    }

    public function removeBan(Ban $ban): self
    {
        // set the owning side to null (unless already changed)
        if ($this->bans->removeElement($ban) && $ban->getUser() === $this) {
            $ban->setUser(null);
        }

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getWhitelisted(): ?bool
    {
        return $this->whitelisted;
    }

    public function setWhitelisted(bool $whitelisted): self
    {
        $this->whitelisted = $whitelisted;

        return $this;
    }


}
