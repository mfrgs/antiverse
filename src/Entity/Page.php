<?php

namespace App\Entity;

use App\Repository\PageRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PageRepository::class)]
class Page
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text')]
    private $title;

    #[ORM\Column(type: 'string', length: 255)]
    private $url;

    #[ORM\Column(type: 'text', nullable: true)]
    private $content;

    #[ORM\Column(type: 'boolean')]
    private $displayInMenu;

    #[ORM\Column(type: 'boolean')]
    private $displayMenu;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getDisplayInMenu(): ?bool
    {
        return $this->displayInMenu;
    }

    public function setDisplayInMenu(bool $displayInMenu): self
    {
        $this->displayInMenu = $displayInMenu;

        return $this;
    }

    public function getDisplayMenu(): ?bool
    {
        return $this->displayMenu;
    }

    public function setDisplayMenu(bool $displayMenu): self
    {
        $this->displayMenu = $displayMenu;

        return $this;
    }
}
