<?php

namespace App\Entity;

use App\Repository\BoardRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BoardRepository::class)]
class Board
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    private $nickname;

    #[ORM\Column(type: 'text', nullable: true)]
    private $header;

    #[ORM\OneToMany(mappedBy: 'board', targetEntity: AntiMessage::class)]
    private $antiMessages;

    #[ORM\Column(type: 'boolean')]
    private $whitelisted;

    public function __construct()
    {
        $this->antiMessages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getHeader(): ?string
    {
        return $this->header;
    }

    public function setHeader(string $header): self
    {
        $this->header = $header;

        return $this;
    }


    /**
     * @return Collection|AntiMessage[]
     */
    public function getAntiMessages(): Collection
    {
        return $this->antiMessages;
    }

    public function getAntiMessagesOrdered():Collection
    {
      $criteria = \Doctrine\Common\Collections\Criteria::create()
          ->orderBy(array('datetime'=> \Doctrine\Common\Collections\Criteria::DESC));
          return $this->antiMessages->matching($criteria);
    }

    public function addAntiMessage(AntiMessage $antiMessage): self
    {
        if (!$this->antiMessages->contains($antiMessage)) {
            $this->antiMessages[] = $antiMessage;
            $antiMessage->setBoard($this);
        }

        return $this;
    }

    public function removeAntiMessage(AntiMessage $antiMessage): self
    {
        // set the owning side to null (unless already changed)
        if ($this->antiMessages->removeElement($antiMessage) && $antiMessage->getBoard() === $this) {
            $antiMessage->setBoard(null);
        }

        return $this;
    }

    public function getWhitelisted(): ?bool
    {
        return $this->whitelisted;
    }

    public function setWhitelisted(bool $whitelisted): self
    {
        $this->whitelisted = $whitelisted;

        return $this;
    }



}
