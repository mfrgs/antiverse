<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

   /**
    * @return Booloean
    */
    public function isBanned($id)
    {
      $now = new \DateTime();
      $req = $this->createQueryBuilder('u')
                  ->select('b.end, b.reason')
                  ->leftJoin('u.bans', 'b')
                  ->where('u.id = :id')
                  ->andWhere('b.end > :now')
                  ->orderBy('b.end', 'DESC')
                  ->setParameter('id', $id)
                  ->setParameter('now', $now)
                  ->getQuery();

      return $req->getResult();
    }


    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
