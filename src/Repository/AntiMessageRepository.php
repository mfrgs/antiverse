<?php

namespace App\Repository;

use App\Entity\AntiMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AntiMessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method AntiMessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method AntiMessage[]    findAll()
 * @method AntiMessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AntiMessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AntiMessage::class);
    }

    // /**
    //  * @return AntiMessage[] Returns an array of AntiMessage objects
    //  */
    public function findInContent($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.content LIKE :val')
            ->setParameter('val', '%'.$value.'%')
            ->orderBy('a.datetime', 'DESC')
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?AntiMessage
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
