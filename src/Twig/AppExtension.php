<?php // src/Twig/AppExtension.php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Twig\TwigFunction;
use Detection\MobileDetect;

class AppExtension extends AbstractExtension
{
    private string $reg_exUrl = "/(http|https|ftp|ftps)\\:\\/\\/[a-zA-Z0-9\\-\\.]+\\.[a-zA-Z]{2,3}(\\/\\S*)?/";
    private string $reg_youtubeUrl = "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user|shorts)\/))([^\?&\"'>]+)/";
    private string $reg_stickers = '#\[(.*?)\]#';
    private string $reg_hastag = '/(#\w+)/u';

    public function getFilters()
    {
        return [
            new TwigFilter('displayLink', [$this, 'displayLink']),
            new TwigFilter('displayStickers', [$this, 'displayStickers']),
            new TwigFilter('displayResponses', [$this, 'displayResponses'])
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('isMobile', [$this, 'isMobile']),
        ];
    }

    public function displayLink($str, $displayYoutube = true){
      if(is_null($str)){
        return $str;
      }

      $str = strip_tags($str);
      $urlsToReplace = $this->parsingText($str, $this->reg_exUrl);
      foreach ($urlsToReplace as $i => $singleUrlsToReplace) {
          if ($displayYoutube && preg_match($this->reg_youtubeUrl,($singleUrlsToReplace))) {
              $last = substr($singleUrlsToReplace, (strrpos($singleUrlsToReplace,'/') + 1));
              $last = str_replace('watch?v=', "",$last);
              $str = str_replace($singleUrlsToReplace,
              '<iframe width="300" src="https://www.youtube.com/embed/'.$last.'" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
                , $str);
          }
          $str = str_replace($singleUrlsToReplace, '<a href="'.$singleUrlsToReplace.'"  target="_blank" >' . $singleUrlsToReplace . "</a> ", $str);
      }

      return $str;
    }

    public function displayStickers($str){
      $package = new Package(new EmptyVersionStrategy());
      $stickers = array();
      $stickersToReplace = $this->parsingText($str, $this->reg_stickers);

      foreach ($stickersToReplace as $i => $singleStickersToReplace) {
          $str = str_replace($singleStickersToReplace, '<img class="sticursed" src="'.$package->getUrl("/images/sticursed/").trim($singleStickersToReplace, '[]').'.png" />', $str);
      }
      return $str;


    }

    public function displayResponses($str)
    {
      $hashtags = $this->parsingText($str, $this->reg_hastag);
      foreach ($hashtags as $i => $hashtag) {
        $str = str_replace($hashtag, '<span class="response-to">'.$hashtag.'</span>', $str);
      }

      return $str;
    }

    public function parsingText($str, $regex) :array
    {
      $iterations = array();

      if (is_null($str) ){
        return $iterations;
      }

      $iterationsToReplace = array();
      if (preg_match_all($regex, $str, $iterations)) {
          $numOfIterationsToReplace = 0;
          $itemsCount = is_countable($iterations[0]) ? count($iterations[0]) : 0;
          for ($i = 0;$i < $itemsCount;$i++) {
              $alreadyAdded = false;
              foreach ($iterations as $j => $iteration) {
                  if ($iteration == $iterations[0][$i]) {
                      $alreadyAdded = true;
                  }
              }
              if (!$alreadyAdded) {
                  $iterationsToReplace[] = $iterations[0][$i];
              }
          }
        }
      return $iterationsToReplace;
    }

    public function isMobile()
    {
      $detect = new MobileDetect();
      return $detect->isMobile();
    }

}
