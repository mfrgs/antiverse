<?php

namespace App\Controller;

use App\Entity\Board;
use App\Entity\AntiMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\AntiMessageType;
use App\Service\FileUploader;
use App\Manager\UserManager;
use App\Manager\AntiMessageManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Contracts\Translation\TranslatorInterface;


class AntiMessageController extends AbstractController
{
  #[Route('/new/{board}/{topicId}', name: 'antiMessage_new', methods: ['GET', 'POST'])]
  public function new(Request $request, EntityManagerInterface $entityManager,
                       FileUploader $uploader,
                       UserManager $userManager,
                       LoggerInterface $logger,
                       TranslatorInterface $translator,
                       int $board,
                       int $topicId = null
                       ): Response
  {
    $topic = new AntiMessage();
    $form = $this->createForm(AntiMessageType::class,
                              $topic,
                              ['action' =>$this->generateUrl('antiMessage_new',
                                          array('board' => $board, 'topicId'=> $topicId)),
                                        'label' => 'search']
                            );
    $form->handleRequest($request);
    $board = $entityManager->getRepository(Board::class)->find($board);
    $topic->setBoard($board);
    $path = $request->server->get('HTTP_REFERER');

    if($form->isSubmitted())
    {

      if($form->isValid() == false){
        foreach ($form->getErrors(true, false) as $error) {
          $this->addFlash('error', $error->current()->getMessage());
         }
         return $this->redirect($path);
      }

      try {
        $response = $userManager->authenticate($request->getClientIp(),$board);

        if($response["message"]){
          $this->addFlash('error', $response["message"]);
          return $this->redirect($path);
        }

        $user = $response["user"];
        $topic->setDatetime(new \DateTime());
        $imageFile = $form->get('image')->getData();
        if($imageFile){
              $filename = $uploader->upload($imageFile);
              $topic->setImage($filename);
              }
          $topic->setUser($user);
          if( $topicId != null){
            $topic->setTopic(false);
            $parent =  $entityManager->getRepository(AntiMessage::class)->find($topicId);
            $responseTo = $parent;
            $responseTo->addResponse($topic);
            $parent->setDatetime(new \DateTime());
            $entityManager->persist($parent);
          }else{
            $topic->setTopic(true);
          }
          $entityManager->persist($topic);
          $entityManager->flush();

      } catch (\Exception $e) {
        $logger->error($e->getMessage());
      }
      return $this->redirect($path);
    }

    return $this->render('antimessage/form.html.twig', [
        'form' => $form->createView(),
    ]);
  }

  //// TODO:  transform delete message in hide message
  #[Route('/delete', name: 'antiMessage_delete', methods: ['POST'])]
   public function delete(Request $request, EntityManagerInterface $entityManager, AntiMessageManager $antiMessageManager): Response
   {

     $arrayId = json_decode($request->get('id'), null, 512, JSON_THROW_ON_ERROR);
     $password = $request->get('password');

       foreach ($arrayId as $id) {
         $antiMessageManager->deleteAntimessages($id,$password);
      }
     $path = $request->server->get('HTTP_REFERER');
     return $this->redirect($path);

   }

   #[Route('/search', name: 'antiMessage_search', methods: ['POST'])]
   public function search(Request $request, EntityManagerInterface $entityManager): Response
   {
     $form = $this->createFormBuilder()
    ->add('search', SearchType::class, array('constraints' => new Length(array('min' => 3)), 'attr' => array('placeholder' => 'Rechercher un message') ))
    ->add('send', SubmitType::class, array('label' => 'chercher'))
    ->setAction($this->generateUrl('antiMessage_search'))
    ->setMethod('POST')
    ->getForm();

    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid())
    {
       $results = $entityManager->getRepository(AntiMessage::class)->findInContent($form['search']->getData());
       return $this->render('antimessage/results.html.twig', [
           'messages' => $results,
       ]);
    }
    return $this->render('antimessage/search.html.twig', [
        'form' => $form->createView(),
    ]);
   }


}
