<?php

namespace App\Controller;

use App\Entity\Ban;
use App\Form\BanType;
use App\Repository\BanRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

#[Route('/admin/ban')]
class BanController extends AbstractController
{
    #[Route('/', name: 'ban_index', methods: ['GET'])]
    public function index(Request $request, BanRepository $banRepository, PaginatorInterface $paginator): Response
    {

      $bans = $banRepository->findBy(array(), array('id' => 'DESC'));

      $pagination = $paginator->paginate(
          $bans, /* query NOT result */
          $request->query->getInt('page', 1), /*page number*/
          10 /*limit per page*/
      );
        return $this->render('ban/index.html.twig', [
            'bans' => $pagination
        ]);
    }

    #[Route('/new', name: 'ban_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $ban = new Ban();
        $form = $this->createForm(BanType::class, $ban, ['action' => $this->generateUrl('ban_new')]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($ban);
            $entityManager->flush();

            return $this->redirectToRoute('ban_index');


        }

        return $this->renderForm('ban/new.html.twig', [
            'ban' => $ban,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'ban_show', methods: ['GET'])]
    public function show(Ban $ban): Response
    {
        return $this->render('ban/show.html.twig', [
            'ban' => $ban,
        ]);
    }

    #[Route('/{id}/edit', name: 'ban_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Ban $ban, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(Ban1Type::class, $ban);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('ban_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('ban/edit.html.twig', [
            'ban' => $ban,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'ban_delete', methods: ['POST'])]
    public function delete(Request $request, Ban $ban, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ban->getId(), $request->request->get('_token'))) {
            $entityManager->remove($ban);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ban_index', [], Response::HTTP_SEE_OTHER);
    }
}
