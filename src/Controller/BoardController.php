<?php

namespace App\Controller;

use App\Entity\Board;
use App\Entity\AntiMessage;
use App\Entity\Page;

use App\Form\BoardType;
use App\Repository\BoardRepository;
use App\Repository\PageRepository;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\AntiMessageType;
use App\Service\FileUploader;
use App\Manager\UserManager;
use Psr\Log\LoggerInterface;
use Knp\Component\Pager\PaginatorInterface;


class BoardController extends AbstractController
{
  #[Route('/', name: 'index', methods: ['GET'])]
  public function index(BoardRepository $boardRepository,PageRepository $pageRepository): Response
  {
    $indexPage = $pageRepository->findOneBy(array("url"=>"index"));

    if($indexPage !== null){
      return $this->render('page/show.html.twig', [
          'page' => $indexPage,
      ]);
    }
    return $this->render('index.html.twig', [
        'boards' => $boardRepository->findAll(),

    ]);

  }

    #[Route('/admin/boards', name: 'board_index', methods: ['GET'])]
    public function list(BoardRepository $boardRepository): Response
    {
        return $this->render('board/index.html.twig', [
            'boards' => $boardRepository->findAll(),
        ]);
    }

    public function menu(BoardRepository $boardRepository, PageRepository $pageRepository): Response
    {
        return $this->render('board/_menu.html.twig', [
            'boards' => $boardRepository->findAll(),
            'pages' => $pageRepository->findBy( array('displayInMenu'=> true))
        ]);
    }

    #[Route('/admin/board/new', name: 'board_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        // ajout formulaire sur la page création de board
        $board = new Board();
        $form = $this->createForm(BoardType::class, $board);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($board);
            $entityManager->flush();

            return $this->redirectToRoute('board_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('board/new.html.twig', [
            'board' => $board,
            'form' => $form,
        ]);
    }

    #[Route('/board/{nickname}', name: 'board_show', methods: ['GET','POST'])]
    public function show(Request $request, EntityManagerInterface $entityManager,
                         Board $board,
                         FileUploader $uploader,
                         UserManager $userManager,
                         LoggerInterface $logger,
                         PaginatorInterface $paginator): Response
    {


      $topics = $entityManager->getRepository(AntiMessage::class)->findBy(['board' => $board,
                                                                           "topic" => true],
                                                                          ['datetime' => 'DESC']);

        //limit messages related to topic on frontpage
        foreach ($topics as $key => $topic) {
          $topics[$key]->setResponses($topic->getResponsesLimit());
        }

        $pagination = $paginator->paginate(
            $topics, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('board/show.html.twig', [
            'board' => $board,
            'antiMessages' => $pagination,
            'new' => true
        ]);
    }

    #[Route('/board/{board}/{id}', name: 'topic_show', methods: ['GET','POST'])]
    public function showTopic(Request $request,  AntiMessage $topic,
                         EntityManagerInterface $entityManager,
                         FileUploader $uploader,
                         UserManager $userManager,
                         LoggerInterface $logger,
                         PaginatorInterface $paginator): Response
    {
      $board = $entityManager->getRepository(Board::class)->findOneBy(['nickname' => $request->get('board')]);

      $pagination = $paginator->paginate(
          $topic->getResponses(), /* query NOT result */
          $request->query->getInt('page', 1), /*page number*/
          10 /*limit per page*/
      );

      return $this->render('board/show.html.twig', [
          'antiMessages' => [$topic],
          'responses' => $pagination,
          'board' => $board,
          'new' => false
      ]);
    }


    #[Route('/admin/board/{id}/edit', name: 'board_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Board $board, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(BoardType::class, $board);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('board_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('board/edit.html.twig', [
            'board' => $board,
            'form' => $form,
        ]);
    }

    #[Route('/admin/board/{id}', name: 'board_delete', methods: ['POST'])]
    public function delete(Request $request, Board $board, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$board->getId(), $request->request->get('_token'))) {
            $entityManager->remove($board);
            $entityManager->flush();
        }

        return $this->redirectToRoute('board_index', [], Response::HTTP_SEE_OTHER);
    }
}
