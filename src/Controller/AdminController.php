<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Ban;
use App\Entity\Admin;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Form\BanType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Knp\Component\Pager\PaginatorInterface;
use App\Manager\UserManager;

#[Route('/admin')]
class AdminController extends AbstractController
{
    #[Route('/', name: 'admin_index', methods: ['GET'])]
    public function index(UserRepository $userRepository, Request $request, PaginatorInterface $paginator): Response
    {
      $users = $userRepository->findBy(array(), array('id' => 'DESC'));
      $pagination = $paginator->paginate(
          $users,
          $request->query->getInt('page', 1),
          20
      );
        return $this->render('admin/index.html.twig', [
            'users' => $pagination,
        ]);
    }


    #[Route('/users/{id}', name: 'admin_show', methods: ['GET'])]
    public function show(User $user): Response
    {
        return $this->render('admin/show.html.twig', [
            'user' => $user,
        ]);
    }


    #[Route('/users/{id}/edit', name: 'admin_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, User $user, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/users/{id}', name: 'admin_delete', methods: ['POST'])]
    public function delete(Request $request, User $user, UserManager $userManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
          $userManager->deleteUser($user);
        }

        return $this->redirectToRoute('admin_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/logout', name: 'app_logout', methods: ['GET'])]
    public function logout(): void
    {
    }

    #[Route('/user/whitelist', name: 'admin_whitelist', methods: ['POST'])]
    public function whitelist(Request $request, UserRepository $userRepository, EntityManagerInterface $entityManager): Response
    {
      $user = $userRepository->find($request->get('id'));
      if($user->getWhitelisted()){
        $user->setWhitelisted(false);
      }
      else {
        $user->setWhitelisted(true);
      }
      $entityManager->persist($user);
      $entityManager->flush();
      return new Response('', Response::HTTP_OK);
    }

}
