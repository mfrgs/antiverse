<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Admin;
use App\Form\AdminType;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
  #[Route('/login', name: 'login', methods :["GET", "POST"])]
  public function login(Request $request, AuthenticationUtils $authenticationUtils): Response
  {
      #/home/mfrgs/Documents/lamp-vagrant-master/www/antiverse/vendor/symfony/security-http/Authentication/AuthenticatorManager.php
      $error = $authenticationUtils->getLastAuthenticationError();
      // last username entered by the user
      $lastUsername = $authenticationUtils->getLastUsername();

      return $this->render('login/index.html.twig', [
        'last_username' => $lastUsername,
        'error'         => $error,
      ]);
  }

  #[Route('/admin/register', name: 'admin_register', methods: ['GET', 'POST'])]
  public function register(Request $request, EntityManagerInterface $entityManager,UserPasswordHasherInterface $passwordHasher) :Response
  {

    $admin = new Admin();

    $form = $this->createForm(AdminType::class, $admin);
    $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid())
    {

      // hash the password (based on the security.yaml config for the $user class)
      $hashedPassword = $passwordHasher->hashPassword($admin, $admin->getPassword());
      $admin->setPassword($hashedPassword);

      $admin->setRoles(["ROLE_ADMIN"]);


      $entityManager->persist($admin);
      $entityManager->flush();
    }
    return $this->renderForm('login/register.html.twig', [
        'form' => $form,
    ]);
  }
}
