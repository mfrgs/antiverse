<?php

namespace App\Form;

use App\Entity\AntiMessage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Gregwar\CaptchaBundle\Type\CaptchaType;

class AntiMessageType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('image',FileType::class,[
          'constraints' => [
            new File([
                'maxSize' => '2000k',
                'mimeTypes' => [
                    'image/gif',
                    'image/jpeg',
                    'image/png',
                ],
                'mimeTypesMessage' => 'use valid image',
            ])
          ],
          'required' => false
        ],
        ['label' => 'antimessages.image'])
            ->add('content',null,['label' => 'antimessages.content', 'attr'=>['maxlength' => 1500]])
            ->add('password', PasswordType::class,['required'   => false, 'label' => 'antimessages.password'])
            ->add('captcha', CaptchaType::class,['label' => 'antimessages.captcha'])
            ->add('envoyer', SubmitType::class, [
                'attr' => ['class' => 'envoyer'],
            ],['label' => 'antimessages.send']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AntiMessage::class,
        ]);
    }
}
