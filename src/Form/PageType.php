<?php

namespace App\Form;

use App\Entity\Page;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title',null,['label' => 'antimessages.title'])
            ->add('url')
            ->add('content', CKEditorType::class, array(
    'config' => array(
        'uiColor' => '#ffffff',
    )))
            ->add('displayInMenu',null,['label' => 'display_in_menu'])
            ->add('displayMenu',null,['label' => 'display_menu'])
            ->add('envoyer', SubmitType::class, [
                'attr' => ['class' => 'envoyer'],
            ],['label' => 'antimessages.send']);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
        ]);
    }
}
