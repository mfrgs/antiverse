<?php

namespace App\Form;

use App\Entity\Ban;
use App\Entity\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class BanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('reason', null, ['label' => 'reason'])
            ->add('start', DateType::class, ['widget' => 'single_text','label' => 'start'])
            ->add('end', DateType::class, ['widget' => 'single_text','label' => 'end'])
            ->add('user', EntityType::class, [
              'class' => User::class,
              'choice_label' => 'name',
              'label' => 'user'
            ])
            ->add('ban', SubmitType::class, [
                'attr' => ['class' => 'ban'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ban::class,
        ]);
    }
}
