<?php

namespace App\Form;

use App\Entity\Board;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class BoardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name',null,['label' => 'name'])
            ->add('nickname',null,['label' => 'nickname'])
            ->add('header', CKEditorType::class, array(
    'config' => array(
        'uiColor' => '#ffffff',
    )))            ->add('whitelisted',null,['label' => 'whitelist'])
            ->add('envoyer', SubmitType::class, [
                'attr' => ['class' => 'envoyer'],
            ],['label' => 'antimessages.send']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Board::class,
        ]);
    }
}
